import React, { Component } from 'react';
import './App.css';

import * as cocoSsd from "@tensorflow-models/coco-ssd";
import "@tensorflow/tfjs";

class App extends Component {
  state = {
    btnDeviceId: null,
    model: null,
    width: 550,
    height: 400,
  }
  videoRef = React.createRef();
  canvasRef = React.createRef();

  componentDidMount() {
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      const webCamPromise = navigator.mediaDevices
        .getUserMedia({
          audio: false,
          video: true
        })
        .then(stream => {
          window.stream = stream;
          this.videoRef.current.srcObject = stream;
          navigator.mediaDevices
          .enumerateDevices()
          .then((devices) => {
            
          });
          return new Promise((resolve, reject) => {
            this.videoRef.current.onloadedmetadata = () => {
              resolve();
            };
          });
        });
      const modelPromise = cocoSsd.load();
      Promise.all([modelPromise, webCamPromise])
        .then(values => {
          this.setState({
            model: 1,
          })

          this.detectFrame(this.videoRef.current, values[0]);
        })
        .catch(error => {
          console.error(error);
        });
    }
  }

  detectFrame = (video, model) => {
    model.detect(video).then(predictions => {
      this.renderPredictions(predictions);
      requestAnimationFrame(() => {
        this.detectFrame(video, model);
      });
    });
  };

  renderPredictions = predictions => {
    const ctx = this.canvasRef.current.getContext("2d");
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    const font = "16px sans-serif";
    ctx.font = font;
    ctx.textBaseline = "top";
    predictions.forEach(prediction => {
      const x = prediction.bbox[0];
      const y = prediction.bbox[1];
      const width = prediction.bbox[2];
      const height = prediction.bbox[3];
      ctx.strokeStyle = "#00FFFF";
      ctx.lineWidth = 4;
      ctx.strokeRect(x, y, width, height);
      ctx.fillStyle = "#00FFFF";
      const textWidth = ctx.measureText(prediction.class).width;
      const textHeight = parseInt(font, 10); 
      ctx.fillRect(x, y, textWidth + 4, textHeight + 4);
    });

    predictions.forEach(prediction => {
      const x = prediction.bbox[0];
      const y = prediction.bbox[1];
      ctx.fillStyle = "#000000";
      ctx.fillText(prediction.class, x, y);
    });
  };
  changeFacingMode = (facingMode) => {
    if (this.videoRef.current.srcObject) {
      this.videoRef.current.srcObject
        .getTracks()
        .forEach((track) => track.stop());

      this.videoRef.current.srcObject = null;
    }

    navigator.mediaDevices
      .getUserMedia({
        video: {
          facingMode: facingMode,
        }
      })
      .then((stream) => this.videoRef.current.srcObject = stream);
  }

  render() {
    return (
      <div className="App">
          <header class="header-fixed">

	<div class="header-limiter">

		<h1><a>Object Detection<span> using React and Tensorflow js</span></a></h1>

		

	</div>

</header>
        <div id="preview">
        <video
          className="fixed"
          autoPlay
          playsInline
          muted
          ref={this.videoRef}
          width="360"
          height="480"
        />
        <canvas
          className="fixed"
          ref={this.canvasRef}
          width="360"
          height="480"
        />
        </div>
      {!this.state.model && (
        <div class="loader" style={{width: this.state.width, height: this.state.height,fontSize:18}}>
          Loading model...
        </div>
      )}
      </div>
    );
  }
}

export default App;
